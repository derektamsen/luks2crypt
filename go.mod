module github.com/square/luks2crypt

require (
	github.com/dselans/dmidecode v0.0.0-20180814053009-65c3f9d81910
	github.com/gorilla/schema v1.1.0
	github.com/sethvargo/go-diceware v0.0.0-20181024230814-74428ac65346
	gopkg.in/urfave/cli.v1 v1.20.0
)
